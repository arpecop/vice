import React, { Component } from 'react'

import { Row, Col } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import contentData from '../utils/contentData'

class Content extends Component {
    state = { items: [] }
    componentDidMount() {
        const query2 = `&skip=${Math.floor(Math.random() * 59979)}`
        fetch(
            `https://pouchdb.herokuapp.com/jokes/_all_docs?include_docs=true&limit=20${query2}`
        )
            .then(response => response.json())
            .then(data => this.setState({ hits: data.rows }))
    }
    render() {
        return (
            <div className="next-steps my-5">
                <h2 className="my-5 text-center">Вицове на деня</h2>
                <Row className="d-flex justify-content-between">
                    {contentData.map((col, i) => (
                        <Col key={i} md={5} className="mb-4">
                            <h6 className="mb-3">
                                <a href={col.link}>
                                    <FontAwesomeIcon
                                        icon="link"
                                        className="mr-2"
                                    />
                                    {col.title}
                                </a>
                            </h6>
                            <p>{col.description}</p>
                        </Col>
                    ))}
                </Row>
            </div>
        )
    }
}

export default Content
